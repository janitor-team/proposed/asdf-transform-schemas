0.2.2 (2022-02-24)
------------------

- Add inputs and outputs to base transform schema to properly document them. [#33]
- Add spline1d schema. [#41]
- Add cosine, tangent, arcsine, arccosine, and arctangent schemas. [#40]
- Fix circular build dependencies for asdf. [#38]

0.2.0 (2021-12-13)
------------------

- Remove generic-1.x.0 schemas and examples. [#30]

0.1.0 (2021-11-24)
------------------

- Initial release
